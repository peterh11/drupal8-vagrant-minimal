#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='12345678'

# update
sudo apt-get update

# install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y libapache2-mod-php php-mcrypt php-mysql php-curl php-gd php-json php-mbstring php-mcrypt

# install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server
sudo mysql -p"$PASSWORD" -e "create database drupal"

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

# setup hosts file
VHOST=$(cat <<EOF
DirectoryIndex index.php index.html

<VirtualHost *:80>
    DocumentRoot "/vagrant_data/web"
    <Directory "/vagrant_data/web">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
sudo sed -i 's/www-data/ubuntu/g' /etc/apache2/envvars
sudo touch /etc/apache2/sites-available/drupal8.conf
sudo ln -s /etc/apache2/sites-available/drupal8.conf /etc/apache2/sites-enabled/drupal8.conf
sudo rm /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/000-default.conf
sudo sh -c "echo \"${VHOST}\" > /etc/apache2/sites-available/drupal8.conf"

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
sudo service apache2 restart

# install git
sudo apt-get -y install git

# install Composer
sudo curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

cd /vagrant_data
cp vagrant/settings.local.php web/sites/default/settings.local.php
cp vagrant/settings.php web/sites/default/settings.php

composer install
./vendor/bin/drush site-install -y
