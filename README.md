# Introduction

It's a minimal Vagrant machine for Drupal 8 development. You can start your encapsulated work environment in a few minutes, with a single command.

# Development

## Stack
* Ubuntu 16.04
* PHP 7
* MySQL 5.7
* Drupal 8

## Requirements
* Vagrant
* Virtualbox

## Instructions
* `vagrant up`
* At the end of the process you should get a message about your admin account (`[success] Installation complete.  User name: admin  User password: password`)
* Done, navigate to `http://192.168.44.77/user/login` and login
